#! /bin/bash

#It displays a help information
 echo "Please enter any directory"
 echo "Let the directory contains the files"
 echo "Please create,delete or modify files to test the script"

# 'X' implies a directory containing files
  X=$1

# 'A' implies previous md5hash of a file

  A="$(find $X -type f -exec md5sum {} \; | sort)"
  hash1="$(echo "$A" | awk '{print $1}')"
  file1="$(echo "$A" | awk '{print $2}')"
    

# 'B' implies current md5hash of a file
# 'C' implies md5hash of a file being modified
# 'D' implies md5hash of a file being created
# 'E' implies md5hash of a file being deleted


while true; do
  sleep 3
  B="$(find $X -type f -exec md5sum {} \; | sort)"
  hash2="$(echo "$B" | awk '{print $1}')"
  file2="$(echo "$B" | awk '{print $2}')"

  
  C="$(comm -13 <(echo "$A") <(echo "$B"))"
  hash3="$(echo "$C" | awk '{print $1}')"
  file3="$(echo "$C" | awk '{print $2}')"
    
  if [ -n "$hash3" ]; then
     if [ "$file1" == "$file2" ] && [ "$hash1" != "$hash2" ]; then
       echo "[*] File $file3 was modified at $(date)"
     fi
  fi
  
 
  D="$(comm -13 <(echo "$A") <(echo "$B"))"
  hash4="$(echo "$D" | awk '{print $1}')"
  file4="$(echo "$D" | awk '{print $2}')"
    
  if [ -n "$hash4" ]; then
    if [ "$file1" != "$file2" ]; then 
      echo "[+] File $file4 was created at $(date)"
    fi
  fi


  E="$(comm -23 <(echo "$A") <(echo "$B"))"
  hash5="$(echo "$E" | awk '{print $1}')"
  file5="$(echo "$E" | awk '{print $2}')"

  if [ -n "$hash5" ]; then
    echo "[-] File $file5 was deleted at $(date)"
  fi


  A="$(echo "$B")"
  
  echo "scripting is success"

done
 
